import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";

String koreaGreeting = "Anyoung Flutter";
String chineseGreeting = "Ni hao Flutter";

String italianGreeting = "Ciao Flutter";
String russianGreeting = "Preevyet Flutter";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.airplane_ticket_outlined)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != koreaGreeting?
                   koreaGreeting :  chineseGreeting;
                  });
                },
                icon: Icon(Icons.train_outlined)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText != italianGreeting?
                    italianGreeting : russianGreeting;
                  });
                },
                icon: Icon(Icons.local_pizza_outlined))
          ],
        ),
        body: Center(
          child: Text(
              displayText,
            style: TextStyle(fontSize: 24),
          ),
        ),
      ),
    );
  }
}







// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(
//                 onPressed: () {},
//                 icon: Icon(Icons.refresh))
//           ],
//         ),
//         body: Center(
//           child: Text(
//               "Hello Flutter !",
//             style: TextStyle(fontSize: 24),
//           ),
//         ),
//       ),
//     );
//   }
// }
